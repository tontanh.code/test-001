let num1 = 1; 
let num2 = 2;

num1 = num1 + num2;  // full value num1 = 3
num2 = num1 - num2;  // num2 = 1
num1 = num1 - num2;   // num1 = 2

console.log("num1:", num1); // Output: num1: 5
console.log("num2:", num2); // Output: num2: 7

