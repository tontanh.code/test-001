
let inputListMax = ['12', '32', '56', '1', '4', '7', '8', '3', '10', '0', 'MaX'];
let inputListMin = ['12', '32', '56', '1', '4', '7', '8', '3', '10', '0', 'Min'];

function raise (data){
    let numbers = data.filter(item => !isNaN(item) && item != 0).map(item=>parseInt(item));
let string = data.filter(item => isNaN(item)).join(' ').toLowerCase();
if(string == 'max'){
  console.log(numbers.sort((a,b)=> b-a).join(' '))
}else{
  console.log(numbers.sort((a,b)=> a-b).join(' '))
}
}

raise(inputListMax);
raise(inputListMin);