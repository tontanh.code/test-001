function isDivisible(a, b) {
  return a % b === 0 ? 0 : 1;
}

console.log(isDivisible(7, 3));  // Output: 1
console.log(isDivisible(9, 3));  // Output: 0
console.log(isDivisible(10, 2)); // Output: 0