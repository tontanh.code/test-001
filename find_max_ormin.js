const input = ["100", "90"];
const numbers = input.map(Number);

const max = Math.max(...numbers);
const min = Math.min(...numbers);

console.log(`MAX : ${max}`);
console.log(`MIN : ${min}`);
console.log(`numbers : ${numbers}`);
