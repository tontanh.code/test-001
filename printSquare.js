function printSquare(size) {
    for (let i = 1; i <= size; i++) {
        if (i === 1 || i === size) {
            console.log('#'.repeat(size));
        } else {
            console.log('#' + ' '.repeat(size - 2) + '#');
        }
    }
}

// Adjust the size as needed
let size = 5;
printSquare(size);
