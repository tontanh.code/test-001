function decryptSentence(sentence) {
  // 
  let words = sentence.split(" ");
  // 
  let reversedWords = words.reverse();

  let decryptedSentence = reversedWords.join(" ");
  return decryptedSentence;
}

// Input 
let encryptedSentence =
  "life. social crumbling a and responsibilities, bills, with except again old years six be to good feel it does DAMN AND Bandicoot Crash played first I when 6 was I";
// 
let decryptedSentence = decryptSentence(encryptedSentence);
// 
console.log(decryptedSentence);
