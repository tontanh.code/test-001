function printDollarPyramid(height) {
  for (let i = 1; i <= height; i++) {
    let space = " ".repeat(height - i);
    let dollars = '$ '.repeat(i).trim();
    console.log(space + dollars);
  }
}

// Adjust the height as needed
let height = 5;
printDollarPyramid(height);
