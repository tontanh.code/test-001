function printPyramid(height) {
    for (let i = 1; i <= height; i++) {
        let space = ' '.repeat(height - i);
        let stars = '*'.repeat(2 * i - 1);
        console.log(space + stars);
    }
}

// Adjust the height as needed
let height = 5;
printPyramid(height);
