grade = (score) => {
  let data;
  switch (true) {
    case score >= 90:
      data = "A";
      break;
    case score >= 80 && score <= 89:
      data = "B";
      break;
    case score >= 60 && score <= 79:
      data = "C";
      break;
    default:
      data = "D";
  }
  return data;
};

const data = grade(60);
console.log(data);
