function isEven(number) {
  return number % 2 === 0;
}
// Example usage:
console.log(isEven(8));
console.log(isEven(123));
